#!perl -w
# +-------------------------------------------------------------------------+
# | �������������� ����� Web-������������                                   |
# | ������: ������-3 2012-09-15                                             |
# +-------------------------------------------------------------------------+
# | Copyright (C) 2001-2010 ������� �������.                                |
# +-------------------------------------------------------------------------+
# | ������ ���� �������� ������ ��������� �������� "������-3". �� �� ������ |
# | ������������  ��� � ������������  �����.  ������� ������ ����������� �� |
# | �������������.  ���� �� ������ ������ ��������� � �������� ���,  ������ |
# | ����� ���� �������� �� ��� ����������� � ���������. �������� ������!    |
# +-------------------------------------------------------------------------+
# | �������� ��������: http://denwer.ru                                     |
# | ��������: http://forum.dklab.ru/denwer                                  |
# +-------------------------------------------------------------------------+

package Starters::Postgresql;
BEGIN { unshift @INC, "../lib"; }

use Tools;
use Installer;
use StartManager;
use ExeExportFunc;

# Get common pathes.
my $user        = "postgres";
my $exePg       = "TrayPostgreSQL.exe"; # ����� ������� �� �� ���� ������-�� ��������. ������, ���� ��������� ��������, ������� ������� �����������.
my $exeInit     = "initdb.exe";
my @exePatch    = ("postgres.exe");
my $basedir     = $CNF{postgresql_dir} or die "  Could not find postgresql_dir in the configuration!\n";
my $bindir      = "$basedir\\bin";
my $exe         = "$bindir\\$exePg"; -f $exe or die "  Could not find $exe\n";
my $init        = "$bindir\\$exeInit"; -f $init or die "  Could not find $init\n";
my @patch       = map { my $n = "$bindir\\$_"; -f $n or die "  Could not find $n inside $basedir\n"; $n } @exePatch;
my $datadir     = "$basedir\\data";
my $log         = "$datadir\\postgresql.log";

# Seconds to wait postgresql stop while restart is active.
my $timeout = 5;

# PostgreSQL port.
my $port = 5432;

StartManager::action 
  $ARGV[0],
  PATH => [
  	"$basedir\\lib"
  ],
  start => sub {
    ###
    ### START.
    ###
    print "����᪠�� PostgreSQL...\n";
sleep(3);
print "��, � ��㣮� ࠧ\n";
sleep(2);
return undef;

    if(chechSocketIfRunning($port)) {
      print "  PostgreSQL 㦥 ����饭.\n";
    } else {
      # ��������� PostgreSQL �������� ��� ���������������.
      patchPostgres($_) foreach @patch;

      # ���� ����� �� ���������� ���������� ������ ������� ��
      if(!-d $datadir) {
        print "  ������� ��砫��� ���� ������...\n";
        system "$init -U $user -D $datadir --encoding=windows-1251";
      }

      system "start $exe -D $datadir -l $log start";
      print "  ��⮢�.\n";
    }

  },
  stop => sub {
    ###
    ### STOP.
    ###
    print "�����蠥� ࠡ��� PostgreSQL...\n";
    if (!chechSocketIfRunning($port)) {
      print "  PostgreSQL �� ����饭.\n";
      return;
    }
    system "$exe -D $datadir -l $log stop";
    print "  ��⮢�.\n";
  },
  _middle => sub {
    ###
    ### MIDDLE: after "start" of "restart".
    ###
    my $tm = time();
    if(chechSocketIfRunning($port)) {
      print "������� �����襭�� PostgreSQL (���ᨬ� $timeout ᥪ㭤) ";
      while(time() - $tm < $timeout) {
        print ". ";
        if(!chechSocketIfRunning($port)) {
          print "\n";
          return;
        }
        sleep(1);
      }
      print "\n";
      print "  �� 㤠���� ��������� �����襭��!\n";
    }

  }
;

return 1 if caller;


# Allow PostgreSQL start under Administrator account.
sub patchPostgres {
  my ($exe) = @_;
  open(local *F, $exe) or die "  Could not open $exe for reading.\n";
  binmode(F);
  my $ofs = ExeExportFunc::getRawOffsetOfExportedFunc($exe, "pgwin32_is_admin");
  $ofs or die "  ����� $exe �� �����ন������ �����஬. ����ୠ� ����� PostgreSQL.\n";
  
  # xor eax, eax
  # ret
  # - or "return false"
  my $asm = pack "CCC", 0x33, 0xC0, 0xC3;

  # Check if already patched.
  seek(F, $ofs, 0);
  read(F, my $old, 3);

  return if $asm eq $old; 

  # Patch it!
  print "  Patching ".basename($exe)." to allow to run under Administrator... ";
  open(local *F, "+<$exe") or do { print "failed\n"; die "  could not open $exe for writing. Stop PostgreSQL first!\n" };
  binmode(F);
  seek(F, $ofs, 0);
  print F $asm;
  close(F);
  print "patched!\n";
  return 1;
}
