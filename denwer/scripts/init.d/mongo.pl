#!perl -w
# +-------------------------------------------------------------------------+
# | �������������� ����� Web-������������                                   |
# | ������: ������-3 2013-06-02                                             |
# +-------------------------------------------------------------------------+
# | Copyright (C) 2001-2010 ������� �������.                                |
# +-------------------------------------------------------------------------+
# | ������ ���� �������� ������ ��������� �������� "������-3". �� �� ������ |
# | ������������  ��� � ������������  �����.  ������� ������ ����������� �� |
# | �������������.  ���� �� ������ ������ ��������� � �������� ���,  ������ |
# | ����� ���� �������� �� ��� ����������� � ���������. �������� ������!    |
# +-------------------------------------------------------------------------+
# | �������� ��������: http://denwer.ru                                     |
# | ��������: http://forum.dklab.ru/denwer                                  |
# +-------------------------------------------------------------------------+
##
## This file contains stub for creating custom starters.
## You may delete it if you want.
##

package Starters::mongo;
BEGIN { unshift @INC, "../lib"; }

use Tools;
use Installer;
use StartManager;


my $basedir = $CNF{mongodb_dir};
my $exe = "$basedir\\mongod.exe";
my $port = 27017;


die "  Could not find $basedir\n" if !-d $basedir;

StartManager::action 
	$ARGV[0],
	PATH => [
	],
	start => sub {
		###
		### START.
		###
		print "����᪠�� mongoDB ...\n";
          if(chechSocketIfRunning($port)) {
            print "  mongoDB 㦥 ����饭.\n";
          } else {
            if(!-f $exe) {
                 print "  �� 㤠���� ���� $exe.\n";
            } else {
#repair
system "$exe --repair --dbpath $basedir\\db >NUL";
sleep(1);

        # Run the server.
              my $cmd = join " ", (
                  "start /min $exe",
                  "--dbpath $basedir\\db"  );

              system $cmd;
              print "  ��⮢�.\n";
            }
          }
	},
	stop => sub {
		###
		### STOP.
		###
		print "�����蠥� mongoDB...\n";
                my @ps = Tools::searchForProcesses($exe);
                if(@ps) {
                  foreach my $ps (@ps) {
                     my $r = kill 6, $ps->{pid};
                  }
                  sleep(4);
      # If some processes haven't finished, do it again
      # with more cruel signal.
                  @ps = Tools::searchForProcesses($exe);
                  foreach my $ps (@ps) {
                      my $r = kill 9, $ps->{pid};
                      print "  Process $ps->{exe} (PID=$ps->{pid}) killed with signal 9\n";
                  };
                  print "  ��⮢�.\n";
                } else {
                     print "  mongoDB �� ����饭.\n";
                }
	},
	_middle => sub {
		###
		### MIDDLE: after "start" or "restart".
		###
                my $tm = time();
                if(chechSocketIfRunning($port)) {
                    print "������� �����襭�� MySQL (���ᨬ� $timeout ᥪ㭤) ";
                    while(time() - $tm < $timeout) {
                       print ". ";
                       if(!chechSocketIfRunning($port)) {
                           print "\n";
                           return;
                       }
                       sleep(1);
                    }
                    print "\n";
                    print "  �� 㤠���� ��������� �����襭��!\n";
                }

	},
;

return 1 if caller;